var express = require('express'),
    ipaddr = require('ipaddr.js'),
    favicon = require('serve-favicon'),
    fs = require('fs'),
    url = require('url'),
    bcrypt = require('bcrypt-nodejs'),
    EventEmitter = require('events'),
    cookieParser = require('cookie-parser'),
  	app = express(),
  	server;

function onReadable(data, req, res){
    var chunk = req.read();
    if(chunk !== null){
        data += chunk;
        if (data.length > 1e4 && res !== undefined){
            res.statusCode = 413;
            res.end("Not enought memory");
        }
    }
    return data;
}

///loger///

function logger(req, res, next){
  var date = new Date(), ip;
  date = date.toLocaleTimeString() + '  ' + date.toLocaleDateString();

  var writelogs = fs.createWriteStream(__dirname + '/log.txt', { flags: 'a+'});
  if(!!req.url){
    try{
      ip = ipaddr.parse(req.ip);
    }catch(e){
      throw new Error('Something went wrong!' + e);
    }

    if(ipaddr.IPv6.isValid(req.ip)){
      ip = ip.toIPv4Address();
    }

    writelogs.write(JSON.stringify({
      method: req.method,
      url: req.url,
      time: date,
      ip: ip.toString(),
      code: res.statusCode
    }) + ',\r\n');

    console.log('method: %s, url: %s, time: %s, ip: %s, code: %s', req.method, req.url, date, ip, res.statusCode);

  }else{
    writelogs.write("time: " + date + " ");
    writelogs.write(req.error ? req.error.stack + ',\r\n' : 'none' + ',\r\n');

    console.log("time: " + date + " ");
    console.log(req.error ? req.error.stack + ',\r\n' : 'none' + ',\r\n');
  }
}

var EventEmitterDBStatus = new EventEmitter;


function queueWriteDB(urlParts, data, type){

  var newCall = null;

  queueWriteDB.queue.push([urlParts, data, type]);

  if(queueWriteDB.queue.length === 1){

    updateDB(urlParts, data, type);

  }

}

queueWriteDB.queue = [];
queueWriteDB.emmitter = (function(EventEmitterDBStatus){

  EventEmitterDBStatus.on('ready', function(){
    var newCall = null;

    if(typeof queueWriteDB.queue[0] !== 'undefined'){
      newCall = queueWriteDB.queue[0];

      if(newCall){
        updateDB.apply(null, newCall);
        newCall = null;
      }
    }



  });

})(EventEmitterDBStatus); 

function initDB(){

  var temp = '';
  var dbStream = fs.createReadStream(__dirname + '/db.json');

    dbStream
      .on('readable', function(){
        temp = onReadable(temp, this);
      })
      .on('end', function(){
        try{

          db = JSON.parse(temp);

        }catch(err){
          logger({error: err});
        }
      })
      .on('error', function(err){
        logger({error: err});
        db = {};
      });
}

function updateDB(urlParts, data, type){
  if(type === 'add' || type === 'edit'){
    urlParts.push(data.name);
  }
  var db_temp = db;
  for(var i = 0; i < urlParts.length; i++){
    if(db_temp[urlParts[i]]){

      // for edit existing or delete
      if(i === urlParts.length-1){6

        //delete
        if(data === 'delete'){


          delete db_temp[urlParts[i]];


          break;
        }
        console.log('?', data, db_temp, urlParts, urlParts[i], db_temp[urlParts[i]]);

        // update only a content of doc file
        if(type === 'update_content'){

          db_temp[urlParts[i]].content = data.content;
          break;

        }

          //edit
        if(db_temp[urlParts[i]].type === 'doc'){
          db_temp[data.nameNew] = {
            type:  db_temp[urlParts[i]].type,
            title: data.title,
            content:  data.content || db_temp[urlParts[i]].content
          };
        }else{
          db_temp[data.nameNew] = db_temp[data.name];
        }

        if(data.name !== data.nameNew){
          delete db_temp[urlParts[i]];
        }

        break;

      }

      db_temp = db_temp[urlParts[i]];

    }else{

      //add
      if(data.type === 'doc'){
        db_temp[urlParts[i]] = {
          type: 'doc',
          title: data.title,
          content: ''
        }
      }else{
        db_temp[urlParts[i]] = {};
      }
    }
  }

  

  var writeDB = fs.createWriteStream(__dirname + '/db.json');
  writeDB.write(JSON.stringify(db));

  writeDB.end(function(){

    queueWriteDB.queue.shift();
    EventEmitterDBStatus.emit('ready');
   
  });
}

//returns path or false

function routesCompare(db, urlParts){
  var obj = urlParts.reduce(function(db, current){
    if(db !== null && db[current]){
      return db[current];
    }else{
      return null;
    }
  }, db);

  var object = {
    path: urlParts.join('/'),
    obj: obj
  };

  return obj !== null ? object : false;

}

function createDataObject(data){

  var dataObject = {};
  
  var urlToEncode = data.path.split('/');
  var urlEncoded = urlToEncode.map(function(a){
    return encodeURIComponent(a);
  });
  data.path = urlEncoded.join('/');

  if(data.obj.type === 'doc'){
  
    dataObject.url = data.path;
    dataObject.title = data.obj.title;
    dataObject.content = data.obj.content;
  }else{
    dataObject.url = data.path;
    dataObject.publications = Object.keys(data.obj).map(function(a){
      if(data.obj[a].type === 'doc'){

        var pubObj = {
          url: data.path + '/' + encodeURIComponent(a),
          title: data.obj[a].title,
          content: data.obj[a].content
        };

        return pubObj;
      }
    });
    dataObject.categories =  Object.keys(data.obj).map(function(a){
      if(data.obj[a].type !== 'doc'){
        
        var catObj = {
          url: data.path + '/' + encodeURIComponent(a),
          title: a
        };

        return catObj;
      }
    });
    dataObject.categories = dataObject.categories.filter(function(item){
      return item ? true : false;
    });
    dataObject.publications = dataObject.publications.filter(function(item){
      return item ? true : false;
    });
  }

  return dataObject;
}

function addInstance(error, obj, urlParts, req){
  if(typeof login.key != 'undefined' && req.cookies.admin === login.key){

    var parent = routesCompare(db, urlParts);
    if(parent !== null && typeof parent != 'undefined'){
      if(typeof parent.obj[obj.name] === 'undefined'){

        queueWriteDB(urlParts, obj, 'add');
        return {success: true};

      }else{

        error.message = 'Error: The name ' + '"' + obj.name + '"' + ' is already used in this location.';
        console.log('Error: The name ' + '"' + obj.name + '"' + ' is already used in this location.');
        return error;

      }
    }else{

      error.message = 'Error: No such route to add';
      return error;

    }
  }else{
    error.message = 'Error: access denied';
    console.log('Error: access denied');
    return error;
  }
}

function editInstance(error, obj, urlParts, req){
  if(typeof login.key != 'undefined' && req.cookies.admin === login.key){

    var parent = routesCompare(db, urlParts);
    if(parent !== null){
      if(typeof parent.obj[obj.name] !== 'undefined'){

        queueWriteDB(urlParts, obj, 'edit');
        return {success: true};

      }else{

        error.message = 'Error: You can not edit something that does not exist';
        console.log('Error: You can not edit something that does not exist');
        return error;

      }

    }else{

      error.message = 'Error: No such route to add';
      return error;
    }
  }else{
    error.message = 'Error: access denied';
    console.log('Error: access denied');
    return error;
  }
}

function deleteInstance(error, obj, req){
  if(typeof login.key != 'undefined' && req.cookies.admin === login.key){

    var urlParts = obj.to_delete.map(function(a){
      return a.split('/');
    });

    urlParts.forEach(function(a){
      var parent = routesCompare(db, a);
      if(parent !== null){

        queueWriteDB(a, 'delete', 'delete');
        error.error = false;


      }else{

        error.message = 'Error: No such route to delete';
        console.log('Error: No such route to delete');
      }
    });

    return error.error === true ? error : {success: true};
  }else{
    error.message = 'Error: access denied';
    console.log('Error: access denied');
    return error;
  }

}

function updateInstanceContent(error, obj, urlParts, req){
  if(typeof login.key != 'undefined' && req.cookies.admin === login.key){
    var parent = routesCompare(db, urlParts);
    if(parent !== null){

      queueWriteDB(urlParts, obj, 'update_content');
      return {success: true};

    }else{

      error.message = 'Error: No such route';
      console.log('Error: No such route');
      return error;
    }
  }else{

    error.message = 'Error: access denied';
    console.log('Error: access denied');
    return error;
  }
}

function login(error, obj, req, res, cb){

  phrase = '',
  secret = '$2a$05$dG61SAy0X9v1zc5nHgBHteVEECf80cOW6rnsH.pZjF2I408tlusvO',
  bcryptRes = '';

  if(typeof obj != 'undefined'){

    phrase = obj.user + obj.password;
    bcrypt.compare(phrase, secret, function(err, match){
      if(match){

        randomValue = Math.random().toString().substr(2, 10);
        login.key = randomValue;


        res.cookie('admin', randomValue, { expires: new Date(Date.now() + 900000), httpOnly: true });
        cb({success: true});

      }else{
        error.message = 'Error: access denied';
        console.log('Error: access denied');
        cb(error);
      }
    });

  }else{
    error.message = 'Error: data not sent';
    console.log('Error: data not sent');
    cb(error);
  }

}

function logout(req, res){
  login.key = '';
  res.cookie('admin', '', { expires: new Date(Date.now())});
  return 'You are logged out!';
}

var db;
initDB();
login.key = '';

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/assets/favicon.ico'));

app.use(cookieParser());

app.use(express.static('public'));
app.use("/projects", express.static('projects'));

//CRUD + login
app.post(/\/add|edit|delete|update_content|login/, function(req, res, next){
    var data = '';
    req
      .on('readable', function(){
        data = onReadable(data, req, res);
      })
      .on('end', function(){
        try{

          var obj = JSON.parse(data);
          var error = {error: true};
          var urlParts = obj.location !== undefined ? obj.location.split('/').slice(3) : null;

          switch(req.url.slice(1)){
            case 'add':
              res.end(JSON.stringify(addInstance(error, obj, urlParts, req)));
              break;

            case 'edit':
              res.end(JSON.stringify(editInstance(error, obj, urlParts, req)));
              break;

            case 'delete':
              res.end(JSON.stringify(deleteInstance(error, obj, req)));
              break;

            case 'update_content':
              res.end(JSON.stringify(updateInstanceContent(error, obj, urlParts, req)));
              break;

            case 'login':
              login(error, obj, req, res, function(obj){
                res.end(JSON.stringify(obj));
              });
              break;

          }

        }catch(err){
          logger({error: err});
          res.statusCode = 400;
          res.end("Bad Request");
          return;
        }
      });
});

app.get('/logout', function(req, res, next){
  
  res.end(JSON.stringify(logout(req, res)));
  next();

}, logger);

app.get('/about', function(req, res, next){
  
  res.end(JSON.stringify("Сайт-портфолио. Здесь вы также найдете заметки по CSS, HTML и JavaScript.Дмитрий Рожков, 2017"));

  next();

}, logger);

app.get('/', function(req, res, next){

  res.redirect('/home');
  next();

}, logger);


app.get('/*', function(req, res, next){

  // triggers for query string modificators
  var edit = false;

  //check if query string exists
  if(Object.keys(req.query).length !== 0){
    for(var i in req.query){
      if(i === 'edit' && req.query[i] === 'true'){
        edit = true;
      }
    }
    console.log('SPLIT', req.url.split('/').splice(req.url.split('/').length-1));
    req.url = req.url.substr(0, req.url.match(/\?/).index);
    console.log(req.url);
  }

  // url parts in series to compare with our route's JSON
  var urlParts = req.url.split('/').slice(1);

  console.log('PARTS', req.url, urlParts);
  urlParts = urlParts.map(function(a){
    console.log('DECODE', decodeURIComponent(a) );
    return decodeURIComponent(a);
  });


  
  var data = routesCompare(db, urlParts);

  if(data !== false){
    var dataObject = createDataObject(data);


    //state

    if(req.cookies.admin === login.key){
      dataObject.admin = true;
    }

    if(data.obj.type !== 'doc'){
      if(typeof login.key != 'undefined' && req.cookies.admin === login.key){
        res.render('admin_list', dataObject);
      }else{
        res.render('list', dataObject);
      }
    }else{
      if(typeof login.key != 'undefined' && edit === true && req.cookies.admin === login.key){
        res.render('admin_item', dataObject);
        edit = false;
      }else{
        dataObject.item_layout = true;
        res.render('item', dataObject);
      }
    }
  }else{
    var err = new Error('No such routes in database.');
    logger({error: err});
    res.end('Not found');
    next();
  }

  // res.render('page', data);
  res.end();
  next();
}, logger);


server = app.listen(80, function(){
	console.log('Server runing...');
});

