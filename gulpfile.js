var gulp         = require('gulp'),
	sass         = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cleanCSS     = require('gulp-clean-css'),
	rename       = require('gulp-rename'),
	browserSync  = require('browser-sync').create(),
	concat       = require('gulp-concat'),
	jade 		 = require('gulp-jade'),
	uglify       = require('gulp-uglify'),
	pump 		 = require('pump'),
	del          = require('del'),
	remember     = require('gulp-remember'),
	sassInherit  = require('gulp-sass-inheritance'),
	jadeInherit  = require('gulp-jade-inheritance'),
	filter       = require('gulp-filter'),
	gulpif       = require('gulp-if'),
	debug		 = require('gulp-debug'),
	maps		 = require('gulp-sourcemaps');

	// for tasks with inheritance
	global.firstRun = true;

// gulp debug alternative

// filter(function (file) {
// 		console.log(1, file.path, file.relative);
// 		var content = [].slice.call(file.contents).reduce(function(x, y){
// 			if(typeof x === 'number'){
// 				return String.fromCharCode((x).toString(10)) + String.fromCharCode((y).toString(10));
// 			}else{
// 				return x + String.fromCharCode((y).toString(10));
// 			}
// 		});
// 		console.log(content.substr(0, 100));
// 		return true;
// 	}),


gulp.task('html', function(cb){
	pump([
			gulp.src(['views/**/*.jade'], {since: gulp.lastRun('html')}),
			// get main files if their partials was changed
			gulpif(function(file){
				
				// no need to find main files if it is a first run
				// we need to compile all files anyway
				// search is costly if it is for all partial files
				// and furthermore, search at first time will give second copy of file that already presents in vynil object
				if(global.firstRun === true){
					return false;
				}
      			return /\/_/.test(file.path) || /^_/.test(file.basename);
      		}, jadeInherit({dir:'views'})),

			//sort out partials
			filter(function (file) {
        		return !/\/_/.test(file.path) || !/^_/.test(file.basename);
      		}),

			jade({
				locals: { 
					url: 'localhost:3000/home/ideas',           
					categories: [],
					publications: [{
						url: 'vanilla_chess',
						title: 'Vanilla Chess',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					},
					{
						url: 'vanilla_chess',
						title: 'Vanilla Chess',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					},
					{
						url: 'vanilla_chess',
						title: 'Vanilla Chess',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					},
					{
						url: 'vanilla_chess',
						title: 'Vanilla Chess',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					},
					{
						url: 'vanilla_chess',
						title: 'Vanilla Chess',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					},
					{
						url: 'vanilla_chess',
						title: 'Vanilla Chess',
					},
					{
						url: 'symfony_helloworld',
						title: 'CRUD for e-commerce tea shop',
					},
					{
						url: 'rubik\'s_cube',
						title: 'Rubik\'s cube simulator',
					}]
				}
  			}),

			remember('html'),

			rename('index.html'),

			gulp.dest('./')
		],
		cb
	);
});

gulp.task('scripts', function(cb){
	pump([
			gulp.src('./src/js/**/*.js', {since: gulp.lastRun('scripts')}),
			// uglify(),
			remember('scripts'),
			gulp.dest('./public')
	    ], 
	    cb
    );
});

gulp.task('styles', function(cb){
	pump([

			gulp.src(['./src/sass/*.{sass,scss}'], {since: gulp.lastRun('styles')}),

      		gulpif(function(file){

      			if(global.firstRun === true){
      				return false;
      			}

      			return /\/_/.test(file.path) || /^_/.test(file.relative);
      		}, sassInherit({dir:'./src/sass'})),

			filter(function (file) {
        		return !/\/_/.test(file.path) || !/^_/.test(file.relative);
      		}),

      		maps.init(),

			sass(),

			autoprefixer({browsers: ['last 15 versions'], cascade: false}),

			cleanCSS({level: {
			    2: {
			      all: false, // sets all values to `false`
			      removeDuplicateRules: true // turns on removing duplicate rules
			    }
 			 }}),

			remember('styles'), // adds files to stream which have not been updated

			debug('styles'),

			concat('main.min.css'),

			debug('styles'),

      		maps.write(),


			gulp.dest('./public'),


		],
		cb
	);
});
gulp.task('styles_base', function(cb){
	pump([
			gulp.src(['./src/sass/*.{sass,scss}'], {since: gulp.lastRun('styles_base')}),

			gulpif(function(file){

      			if(global.firstRun === true){
      				return false;
      			}

      			return /\/_/.test(file.path) || /^_/.test(file.relative);
      		}, sassInherit({dir:'./src/sass'})),

			filter(function (file) {
        		return !/\/_/.test(file.path) || !/^_/.test(file.relative); 
      		}),

			filter(function (file) {
        		return /item_base_layout.sass/.test(file.relative); 
      		}),

			sass(),

			autoprefixer({browsers: ['last 15 versions'], cascade: false}),

			cleanCSS({level: {
			    2: {
			      all: false, // sets all values to `false`
			      removeDuplicateRules: true // turns on removing duplicate rules
			    }
 			 }}),

			remember('styles_base'), // adds files to stream which have not been updated

			concat('base.min.css'),

			gulp.dest('./public'),
		],
		cb
	);
});

gulp.task('assets', function(cb){
	pump([
			gulp.src('./src/assets/**/*', {since: gulp.lastRun('assets')}),
			gulp.dest('./public/assets'),
		],
		cb
	);
});

gulp.task('clean', function(cb){
	return del('./public', cb);
});


// tracks changes in public and reload browser
// this task never ends
gulp.task('sync', function(cb) {
	browserSync.init({
		proxy: "localhost",
		// server: {
		// 	baseDir: './'
		// },
		notify: false
	});

	browserSync.watch(['./public/**/*.*', './index.html']).on('change', browserSync.reload);
	cb();
});


// tracks changes in src and execute other build tasks
// this task never ends
gulp.task('watch', function(cb){
	global.firstRun = false;
	gulp.watch('./src/sass/*.{sass,scss}', gulp.series('styles', 'styles_base'));
	gulp.watch('./src/js/**/*.js', gulp.series('scripts'));
	gulp.watch('./assets/*.*', gulp.series('assets'));
	gulp.watch('./views/**/*.jade', gulp.series('html'));

	cb();
});
// no html
gulp.task('build', gulp.series('clean', gulp.parallel('scripts', 'styles', 'assets', 'html'), 'styles_base'));

// using parallel here cause watch and server never ends
gulp.task('dev', gulp.series('build', gulp.parallel('watch', 'sync')));