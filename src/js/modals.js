var modal_note = document.querySelector('.modal-notify__portfolio'),
	escCallbackToRemoveOnClose;

function display(obj){
	Array.prototype.forEach.call(document.querySelectorAll('.modal__block'), function(a){
		a.classList.remove('modal__block');
	});
	obj.classList.add('modal__block');
}

function modal_hide(){
	modal_note.classList.remove('modal__block');
	if(modal_note.querySelector('.modal__author')){
		modal_note.removeChild(modal_note.querySelector('.modal__author'));
	}
}

function modal_notify(message, leaveItOnPage){
	var returnObj = {
		showAuthor: function(author){
			var p = document.createElement('p');
			p.textContent = "Дмитрий Рожков, 2017";
			p.classList.add('modal__author');
			modal_note.appendChild(p);
			return returnObj;
		}
	};
	Array.prototype.forEach.call(document.querySelectorAll('.modal__block'), function(a){
		a.classList.remove('modal__block');
	});
	modal_note.classList.add('modal__block');
	modal_note.querySelector('.modal__note').innerHTML = message;
	if(!leaveItOnPage){
		var timeout = setTimeout(function(){
			modal_hide();
		}, 5000);
	}
	modal_note.querySelector('.modal__close').addEventListener('click', function(){
		clearTimeout(timeout);
	});
	function closeOnEsc(modal){
		var fn = function(e){
			if(e.keyCode === 27){
				modal_hide();
			}
			clearTimeout(timeout);
			document.body.removeEventListener('keydown', fn);
		}
		escCallbackToRemoveOnClose = fn;
		return fn;
	}

	document.body.addEventListener('keydown', closeOnEsc(modal_note));
	return returnObj;
}





modal_note.querySelector('.modal__close').addEventListener('click', function(){
	modal_hide();
	document.body.removeEventListener('keydown', escCallbackToRemoveOnClose);
});
