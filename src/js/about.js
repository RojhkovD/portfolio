(function(){
	document.querySelector('.about_link').addEventListener('click', function(e){
		e.preventDefault();
		var xhr = new XMLHttpRequest();
		xhr.open('GET', '/about');
		xhr.send();
		xhr.onload = function(){
			var res = xhr.responseText;
			try{
				res = JSON.parse(res).split('.');
				var author = res.pop();
				res = res.join('.');
				res = res + '.';
				modal_notify(res, true).showAuthor(author);
			}catch(err){
				modal_notify(err.message);
			}
		};
		xhr.onerror = function(err){
			modal_notify(err.message);
		};
		
	});
}());