(function(){
	var closeOnEscCbLogin;

	function closeOnEsc(modal){
		var fn = function(e){
			e.stopPropagation();
			if(e.keyCode === 27){
				modal.classList.remove('modal__block');
				document.removeEventListener('keydown', fn);
			}
		};
		closeOnEscCbLogin = fn;
		return fn;
	};

	var modal_login = document.querySelector('.modal-login__portfolio');

	function validatedObjWrap(data, field){
		if(typeof data == 'object' && data !== null){
			return {
				valid: false,
				field: field,
				message: data.message
			}
		}else{
			return {
				valid: true,
				field: field
			}
		}
	}

	document.querySelector('.login-btn').addEventListener('click', function(e){
		e.preventDefault();
		if(this.dataset.login === "true"){
			var xhr = new XMLHttpRequest();
			xhr.open('GET', '/logout');
			xhr.onload = function(){
				try{
					var res = JSON.parse(xhr.responseText);
					modal_notify(res);
					setTimeout(function(){
						location.href = location.href;
					}, 1000);
				}catch(err){
					modal_notify(err.message);
				}
			};
			xhr.onerror = function(){
				modal_notify('Network error');
			};
			xhr.send();
		}else{
			document.addEventListener('keydown', closeOnEsc(modal_login));
			display(modal_login);

			modal_login.querySelector('.modal__close').addEventListener('click', function(){
					modal_login.classList.remove('modal__block');
					document.removeEventListener('keydown', closeOnEscCbLogin);
				});

			modal_login.querySelector('.modal-login__submit').addEventListener('click', function(){

				var user = validate('user', modal_login.querySelector('.modal-login__user').value);
				var password = validate('password', modal_login.querySelector('.modal-login__password').value);


				var valid = true;
				var arr = [validatedObjWrap(user, 'user'), validatedObjWrap(password, 'password')];

				arr.forEach(function(a){
					if(a.valid === false){
						modal_login.querySelector('.modal-login__' + a.field + ' ~ .modal-login__label-error').innerHTML = a.message;
						valid = false;
					}else{
						modal_login.querySelector('.modal-login__' + a.field + ' ~ .modal-login__label-error').innerHTML = '';
					}
				});


				if(valid === true){

					var obj = {
						user: user,
						password: password
					};

					modal_login.classList.remove('modal__block');
					document.removeEventListener('keydown', closeOnEscCbLogin);

					var xhr = new XMLHttpRequest();
					xhr.open('POST', '/login');
					xhr.send(JSON.stringify(obj));
					xhr.onload = function(){
						try{
							var res = JSON.parse(xhr.responseText);
							if(res.error){
								modal_notify(res.message);
							}else{
								modal_notify('Access granted!');
								setTimeout(function(){
									location.href = location.href;
								}, 1000);
							}
						}catch(err){
							modal_notify(err.stack);
						}
					};
					xhr.onerror = function(){
						modal_notify('Network error');
					};
				}
			});
		}
	});

})();