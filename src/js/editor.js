(function(func){

    // CodeMirror is a global variable
    window.myCodeMirror = func(CodeMirror);

})(function(CodeMirror){
        var myCodeMirror = CodeMirror.fromTextArea(document.querySelector('.block_editor'), {
            mode: 'htmlmixed',
            scrollbarStyle: 'overlay',
            lineNumbers: true,
            extraKeys: {
                "F11": function(cm) {
                  cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                  if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            }
         });

        myCodeMirror.setValue(document.querySelector('#data-container').dataset.content);

        document.querySelector('.btn-fullscreen__portfolio').addEventListener('click', function(){
            myCodeMirror.setOption('fullScreen', !myCodeMirror.getOption('fullScreen'));
            this.style.bottom = myCodeMirror.getOption('fullScreen') ? '0px' : '';
        });



        var myModeSpec = {
            name: "htmlmixed",
            tags: {
                style: [["type", '/^text/(x-)?scss$/', "text/x-scss"],
                        [null, null, "css"]],
                custom: [[null, null, "customMode"]]
            }
        }

        return myCodeMirror;
});



