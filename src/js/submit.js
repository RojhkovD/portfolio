(function(func){

	// myCodeMirror is a global
	func(myCodeMirror);

})(function(myCodeMirror){

	var modal_note = document.querySelector('.modal-notify__portfolio');

	function modal_notify(message){
		modal_note.classList.add('modal__block');
		modal_note.querySelector('.modal__note').innerHTML = message;
		var timeout = setTimeout(function(){
			modal_note.classList.remove('modal__block');
		}, 5000);
		modal_note.querySelector('.modal__close').addEventListener('click', function(){
			clearTimeout(timeout);
		});

		Array.prototype.forEach.call(document.querySelectorAll('.modal-notify__portfolio'), function(a){

			document.body.addEventListener('keydown', function(e){
				if(e.keyCode === 27){
					a.classList.remove('modal__block');
				}
				clearTimeout(timeout);
			});
		});
	}

	function submit(){

		var locationWithoutQuery = decodeURIComponent(window.location.href).substr(0, decodeURIComponent(window.location.href).match(/\?/).index);
		// arguments = Array.prototype.decodeURIComponent(window.location.href)slice.call(arguments);
		var object = {
			location: locationWithoutQuery,
			content: myCodeMirror.getValue()		
		};
		var xhr = new XMLHttpRequest;
		xhr.open('POST', '/update_content');
		xhr.send(JSON.stringify(object));
		xhr.onload = function(){
			try{
				var res = JSON.parse(xhr.responseText);
				if(res.error){
					modal_notify(res.message);
				}else{
					modal_notify('Saved!');
				}
			}catch(err){
				modal_notify(err.message);
			}
		};
		xhr.onerror = function(){
			modal_notify('Network error');
		};

	};

	document.querySelector('#editor-submit').addEventListener('click', function(){
	     submit();
	});

});