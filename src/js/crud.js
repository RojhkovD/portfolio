(function(){
	var modal_add = document.querySelector('.modal-add__portfolio');
	var modal_delete = document.querySelector('.modal-delete__portfolio');
	var modal_edit = document.querySelector('.modal-edit__portfolio');
	
	var add = document.querySelector('.button__add');
	var del = document.querySelector('.button__delete');
	var closeOnEscCbs = {};

	function getCloseOnEscCbs(key){
		return closeOnEscCbs[key];
	}

	add.addEventListener('click', function(){
		display(modal_add);
		document.addEventListener('keydown', closeOnEsc(modal_add));
	});

	del.addEventListener('click', function(){
		display(modal_delete);
		document.addEventListener('keydown', closeOnEsc(modal_delete));
	});	


	modal_add.querySelector('.modal__cancel').addEventListener('click', function(){
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_add.className));
		modal_add.classList.remove('modal__block');
	});
	modal_delete.querySelector('.modal__cancel').addEventListener('click', function(){
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_delete.className));
		modal_delete.classList.remove('modal__block');
	});
	modal_edit.querySelector('.modal__cancel').addEventListener('click', function(){
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_edit.className));
		modal_edit.classList.remove('modal__block');
	});

	modal_add.querySelector('.modal__close').addEventListener('click', function(){
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_add.className));
		modal_add.classList.remove('modal__block');
	});
	modal_delete.querySelector('.modal__close').addEventListener('click', function(){
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_delete.className));
		modal_delete.classList.remove('modal__block');
	});
	modal_edit.querySelector('.modal__close').addEventListener('click', function(){
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_edit.className));
		modal_edit.classList.remove('modal__block');
	});

	function closeOnEsc(modal){
		var fn = function(e){
			e.stopPropagation();
			if(e.keyCode === 27){
				modal.classList.remove('modal__block');
				document.removeEventListener('keydown', fn);
			}
		};
		closeOnEscCbs[modal.className] = fn;
		return fn;
	};

	Array.prototype.forEach.call(document.querySelectorAll('.modal-delete, .modal-add, .modal-edit'), function(a){

		document.body.addEventListener('keydown', function(e){
			if(e.keyCode === 27){
				a.classList.remove('modal__block');
			}
		});
	});


	//delete
	modal_delete.querySelector('.modal-delete__submit').addEventListener('click', function(){
		modal_delete.classList.toggle('modal__block');
		document.removeEventListener('keydown', getCloseOnEscCbs(modal_delete.className));

		var checkboxes = Array.prototype.slice.call(document.querySelectorAll('.check_for_delete'));
		var checked = [];
		checked = checkboxes.reduce(function(a, b){
			if(b.checked === true){
				a.push(b.value);
			}
			return a;				
		}, checked);


		var obj = {
			to_delete: checked
		};
		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/delete');
		xhr.send(JSON.stringify(obj));
		xhr.onload = function(){
				try{
					var res = JSON.parse(xhr.responseText);
					if(res.error){
						modal_notify(res.message);
					}else{
						modal_notify('Succesfully deleted!');
						modal_delete.classList.remove('modal_block');
						setTimeout(function(){
							window.location.assign(window.location.href);
						}, 3000)
					}
				}catch(err){
					modal_notify(err.stack);
				}
			};
		xhr.onerror = function(){
			modal_notify('Network error');
		};
	});












	//add
	// check what need to add: directory or document. Document has a title.
	var currentType = modal_add.querySelector('.modal-add__type[checked]');
	Array.prototype.forEach.call(modal_add.querySelectorAll('.modal-add__type'), function(a){
		document.querySelector('label[for=' + a.id + ']').addEventListener('click', function(){
			if(this !== currentType){
				currentType = this;
				console.log(this, currentType);
			}
			if(a.value === 'doc'){
				modal_add.querySelector('.modal-add__title').disabled = false;
				modal_add.querySelector('.modal-add__title').parentElement.parentElement.style.display = 'block';
			}else if(a.value === 'dir'){
				modal_add.querySelector('.modal-add__title').disabled = true;
				modal_add.querySelector('.modal-add__title').parentElement.parentElement.style.display = 'none';
			}
		});
	});

	modal_add.querySelector('.modal-add__submit').addEventListener('click', function(tyoe, name, title){

		//take off error messages
		Array.prototype.forEach.call(modal_add.querySelectorAll('.modal-add__label-error'), function(a){
			console.log(a);
			a.innerHTML = '';
		});

		var type = validate('type', modal_add.querySelector('.modal-add__type:checked').value);
		var name = validate('name', modal_add.querySelector('.modal-add__name').value);
		var title = validate('title', modal_add.querySelector('.modal-add__title').value);

		console.log(type, name, title);
		var valid = true;

		var arr = type === 'doc' ? [type, name, title] : [type, name];

		console.log(arr);
		// error output
		arr.forEach(function(a){
			if(typeof a == 'object'){
				modal_add.querySelector('.modal-add__' + a.field + ' ~ .modal-add__label-error').innerHTML = a.message;
				valid = false;
				modal_add.querySelectorAll('.modal-add__' + a.field).value = '';
				if(a.field === 'type'){
					modal_add.querySelectorAll('.modal-add__type')[0].value = 'doc';
					modal_add.querySelectorAll('.modal-add__type')[1].value = 'dir';
				}
			}
		});
		var dataAdd = {
			location: decodeURIComponent(window.location.href),
			type: type,
			name: name,
		}

		if(type === 'doc'){
			dataAdd.title = title;
		}

		if(valid === true){
			var xhr = new XMLHttpRequest();
			xhr.open('POST', '/add');
			xhr.send(JSON.stringify(dataAdd));
			xhr.onload = function(){
				try{
					var res = JSON.parse(xhr.responseText);
					if(res.error){
						modal_notify(res.message);
					}else{
						modal_add.classList.remove('modal_block');
						document.removeEventListener('keydown', getCloseOnEscCbs(modal_add.className));
						modal_notify('Succesfully added!');
						setTimeout(function(){
							window.location.assign(window.location.href + '/' + encodeURIComponent(dataAdd.name) + '?edit=true');
						}, 3000);
					}
				}catch(err){
					modal_notify(err.stack);
				}
			};

			xhr.onerror = function(){
				modal_notify('Network error!');
			};
		}
	});










	// edit
	var oldName = '';
	Array.prototype.forEach.call(document.querySelectorAll('.edit_container'), function(a){
		a.addEventListener('click', function(){
			modal_edit.classList.add('modal__block');
			document.addEventListener('keydown', closeOnEsc(modal_edit));
			var that = this;
			oldName = that.dataset.name;

			// set data type of entity to edit
			[].forEach.call(modal_edit.querySelectorAll('.modal-edit__type'), function(b){
				if(b.value === that.dataset.type){
					b.checked = true;
				}
				b.disabled = true;
			});


			// name
			modal_edit.querySelector('.modal-edit__name').value = that.dataset.name;

			//title if type is doc
			if(that.dataset.title !== null){
				modal_edit.querySelector('.modal-edit__title').disabled = false;
				modal_edit.querySelector('.modal-edit__title').style.display = 'block';
				modal_edit.querySelector('.modal-edit__title').value = that.dataset.title;
			}
		});
	});

	modal_edit.querySelector('.modal-edit__submit').addEventListener('click', function(){

		//take off error messages
		Array.prototype.forEach.call(modal_edit.querySelectorAll('.modal-edit__label-error'), function(a){
			a.innerHTML = '';
		});


		var type = validate('type', modal_edit.querySelector('.modal-edit__type').value);
		var name = validate('name', modal_edit.querySelector('.modal-edit__name').value);
		var title = validate('title', modal_edit.querySelector('.modal-edit__title').value);

		var valid = true;

		var arr = type === 'doc' ? [type, name, title] : [type, name];

		// error output
		arr.forEach(function(a){
			if(typeof a == 'object'){
				modal_edit.querySelector('.modal-edit__' + a.field + ' ~ .modal-edit__label-error').innerHTML = a.message;
				valid = false;
				[].forEach.call(modal_edit.querySelectorAll('.modal-edit__' + a.field), function(c){
					c.value = '';
				});
				if(a.field === 'type'){
					modal_edit.querySelectorAll('.modal-edit__type')[0].value = 'doc';
					modal_edit.querySelectorAll('.modal-edit__type')[1].value = 'dir';
				}
			}
		});

		var dataEdit = {
			location: decodeURIComponent(window.location.href),
			name: oldName,
			type: type,
			nameNew: name,
		}

		if(type === 'doc'){
			dataEdit.title = title;
		}

		if(valid === true){
			var xhr = new XMLHttpRequest();
			xhr.open('POST', '/edit');
			xhr.send(JSON.stringify(dataEdit));
			xhr.onload = function(){
				try{
					var res = JSON.parse(xhr.responseText);
					if(res.error){
						document.removeEventListener('keydown', getCloseOnEscCbs(modal_edit.className));
						modal_notify(res.message);
					}else{
						document.removeEventListener('keydown', getCloseOnEscCbs(modal_edit.className));
						modal_edit.classList.toggle('modal_none');
						modal_notify('Changed!');
						setTimeout(function(){
							window.location.assign(window.location.href + '/' + encodeURIComponent(dataEdit.nameNew) + '?edit=true');
						}, 3000);
					}
				}catch(err){
					document.removeEventListener('keydown', getCloseOnEscCbs(modal_edit.className));
					modal_notify(err.stack);
				}
			};
			xhr.onerror = function(){
				document.removeEventListener('keydown', getCloseOnEscCbs(modal_edit.className));
				modal_notify('Network error!');
			};
		}
	});
})();