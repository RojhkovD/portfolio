function validate(fieldType, data){
	console.log(fieldType, data);
	var reg = '';
	var error = {};
	switch(fieldType){
		case 'type':
			if(!(data === 'doc' || data === 'dir')){
				error.message = 'Incorrect type';
				error.field = 'type';
				return error;
			}
			break;
		case 'name':
			reg =  /^[-a-zA-Zа-яА-Я0-9@:%_'\+.~#?&! ]{2,256}$/i;
			if(!data.match(reg)){
				error.message = 'Incorrect name';
				error.field = 'name';
				return error;
			}
			break;
		case 'title':
			reg = /^[^<>]{3,256}$/;
			if(!data.match(reg)){
				error.message	 = 'Incorrect title';
				error.field = 'title';
				return error;
			}
			break;
		case 'user':
			reg = /^[a-zA-Z0-9]{5,12}$/;
			if(!data.match(reg)){
				error.message = 'Incorrect username'
				error.field = 'user';
				return error;
			}
			break;
		case 'password':
			reg = /^[a-zA-Z0-9\*]{6,12}$/;
			if(!data.match(reg)){
				error.message = 'Incorrect password';
				error.field = 'password';
				return error;
			}
			break;
	}
	return data;
}