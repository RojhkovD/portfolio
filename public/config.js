requirejs.config({
    urlArgs: "bust=" + (new Date()).getTime(),
    baseUrl: '/lib',
    packages: {
        name: "codemirror",
        location: "/cm",
        main: "lib/codemirror"
    },
    paths: {
        'jquery': 'jquery.min',
        'jquery_scroll_plugin': 'scroll/jquery.scroll.plugin',
        'scroll': 'scroll/jquery.mCustomScrollbar',
        'editor_scroll': '../editor_scroll',
        'nav_scroll': '../nav_scroll',
        'list_scroll': '../list_scroll',
        'editor': '../editor',
        'submit': '../submit',
        'crud': '../crud',
        'menu': '../menu',
        'modals': '../modals',
        'login': '../login',
        'about': '../about',
        'form_validator': '../form_validator'
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'jquery_scroll_plugin': {
            deps: ['jquery']
        },
        'scroll': {
            deps: ['jquery', 'jquery_scroll_plugin'],
            exports: 'mCustomScrollbar'
        },
        'nav_scroll': {
            deps: ['jquery', 'scroll']
        },
        'list_scroll': {
            deps: ['jquery', 'scroll']
        },
        'editor': {
            deps: ['cm/lib/codemirror'],
            exports: 'editor',
        },
        'submit': {
            deps: ['editor']
        },
        'modals': {

        },
        'crud': {
            deps: ['form_validator', 'modals',]
        },
        'login': {
            deps: ['form_validator', 'modals']
        },
        'about': {
            deps: ['form_validator', 'modals']
        },
        'menu': {

        },
        'form_validator': {

        }



    },


});



       