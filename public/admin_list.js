require(['config'], function() {


    // Configuration loaded now, safe to do other require calls
    // that depend on that config.

    
    require(['menu'], function(mCustomScrollbar) {

    });

    require(['nav_scroll'], function(mCustomScrollbar) {

	});

    require(['list_scroll'], function(mCustomScrollbar) {

    });

    require(['about'], function() {

    });

	require(['crud'], function(){
        
	});

    require(['login'], function(){
           
    });

});
