require(['config'], function() {


    // Configuration loaded now, safe to do other require calls
    // that depend on that config.

    
    require(['menu'], function(mCustomScrollbar) {

    });

    require(['nav_scroll'], function(mCustomScrollbar) {

	});

    require(['about'], function() {

    });

    require(['modals'], function(){
           
    });

    require(['login'], function(){
           
    });

	require(['cm/lib/codemirror', 'cm/mode/javascript/javascript', 'cm/mode/css/css', 'cm/mode/xml/xml', 'cm/mode/htmlmixed/htmlmixed', 'cm/addon/display/fullscreen', 'cm/addon/scroll/simplescrollbars'], function(CodeMirror){

    		window.CodeMirror = CodeMirror;

    		require(['editor'], function(){
                
                require(['submit'], function(){

                });

    		});

	    	

	});

});

//'cm_main', 'cm_css', 'cm_xml', 'cm_js', 'cm_htmlmixed', 'cm_fullscreen', 'editor',

//CodeMirror,

// 'nav_scroll', 'cm/lib/codemirror', , 'editor'