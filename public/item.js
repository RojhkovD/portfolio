require(['config'], function() {


    // Configuration loaded now, safe to do other require calls
    // that depend on that config.

    require(['jquery'], function( $ ) {

    	// to prevent redefinition of jQuery by ajax.googleapis async request or any 

    	window.$jq = $.noConflict(true);
    	
	});

    require(['menu'], function() {

	});

	require(['login'], function() {

	});

	require(['about'], function() {

	});

    require(['nav_scroll'], function() {

	});

});
