(function(){
	document.querySelector('.main-nav-btn').addEventListener('click', function(){
		document.querySelector('.main-nav').classList.toggle('menu-block');
	});
	var throttle = function(type, name, obj) {
        obj = obj || window;
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
             requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };

    throttle("resize", "optimizedResize");
    
	window.addEventListener('optimizedResize', function(){
		if(document.documentElement.clientWidth > 402){
			document.querySelector('.main-nav').classList.remove('menu-block');
		}
	});
})();