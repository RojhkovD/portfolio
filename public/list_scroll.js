(function(){
	$jq('.item_container').mCustomScrollbar({
		axis: 'yx',
		theme: 'minimal-dark',
		mouseWheel: {
			enable: true,
			scrollAmount: 150,
		}
	});
})();